package br.com.b2w.tasks.hibernate.solution;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
class TaskService {

	@Autowired
	private TaskRepository taskRepository;
	
	Task findAndUpdateTask(Task requestTask) {
		
		if (requestTask != null && requestTask.getDescription() == null) {
			throw new TaskDescriptionRequiredException("Task description is required");
		}
		
		Optional<Task> tarefas = taskRepository.findById(requestTask.getId());
		
		if (tarefas != null && tarefas.isPresent()) {
			Task tarefa = tarefas.get();
			
			tarefa.setDescription(requestTask.getDescription());
			tarefa.setPriority(requestTask.getPriority());
			
			taskRepository.save(tarefa);
			
			requestTask = tarefa;
			
			return requestTask;
			
		} else {
			throw new TaskNotFoundException("Cannot find task with given id");
		}
	}
}
