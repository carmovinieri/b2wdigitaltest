package br.com.b2w.tasks.hibernate.solution;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

interface TaskRepository extends JpaRepository<Task, Long> {

	Optional<Task> findById(Long id);
}
