package br.com.b2w.tasks.hibernate.solution;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class StartApplication implements CommandLineRunner {

	private static final Logger log = LoggerFactory.getLogger(StartApplication.class);
	
	@Autowired
	private TaskRepository tasks;
	
	public static void main(String[] args) {
		SpringApplication.run(StartApplication.class, args);
	}
	
	@Override
	public void run(String... strings) {
		log.info("Inicializa aplicacao...");
		
		tasks.save(new Task("Task 1", 1l));
		tasks.save(new Task("Task 2", 7l));
		tasks.save(new Task("Task 3", 5l));
		
		System.out.println("\nfindAll");
		tasks.findAll().forEach(x -> System.out.println(x));
	}
}
