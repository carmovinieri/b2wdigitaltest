package br.com.b2w.tasks.hibernate.solution;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.BAD_REQUEST)
class TaskDescriptionRequiredException extends RuntimeException {
	
	private static final long serialVersionUID = -629549844895632353L;

	public TaskDescriptionRequiredException(String message) {
		super(message);
	}
}
