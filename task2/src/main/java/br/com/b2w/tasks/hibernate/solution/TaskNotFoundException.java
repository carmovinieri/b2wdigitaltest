package br.com.b2w.tasks.hibernate.solution;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)
class TaskNotFoundException extends RuntimeException {

	private static final long serialVersionUID = -1570867984726336596L;

	public TaskNotFoundException(String message) {
		super(message);
	}
}
