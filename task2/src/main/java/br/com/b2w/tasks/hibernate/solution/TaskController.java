package br.com.b2w.tasks.hibernate.solution;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
class TaskController {
	
	@Autowired
	private TaskService taskService;
	
	@RequestMapping(path = "/tasks/{id}", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE, 
			produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Task> findById(@PathVariable(value = "id") long id, 
			@Valid @RequestBody Task requestTask) {
		requestTask.setId(id);
		try {
			return new ResponseEntity<Task>(taskService.findAndUpdateTask(requestTask), HttpStatus.OK);
		} catch (TaskNotFoundException tnfe) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		} catch (TaskDescriptionRequiredException tdre) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
	}
}
