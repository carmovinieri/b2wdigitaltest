-- Criando a tabela line
CREATE TABLE `line` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `weight` int(11) NOT NULL,
  `turn` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `turn_UNIQUE` (`turn`)
)
-- Efetuando os inserts
INSERT INTO LINE (name, weight, turn) VALUES ('James Elephant', 500, 6);
INSERT INTO LINE (name, weight, turn) VALUES ('Thomas Jefferson', 400, 3);
INSERT INTO LINE (name, weight, turn) VALUES ('Will Johnliams', 200, 4);
INSERT INTO LINE (name, weight, turn) VALUES ('John Adams', 350, 2);
INSERT INTO LINE (name, weight, turn) VALUES ('Thomas Jefferson', 175, 5);
INSERT INTO LINE (name, weight, turn) VALUES ('George Washington', 200, 1);
-- Criando stored procedure para executar a tarefa
DELIMITER $$
CREATE PROCEDURE sp_lastperson_elevator()
BEGIN
	DECLARE soma INT DEFAULT 0;
	DECLARE nome VARCHAR(100) DEFAULT "";
    DECLARE peso INT DEFAULT 0;
	DECLARE nameReturn VARCHAR(100) DEFAULT "";

	DECLARE line CURSOR FOR
		SELECT name, weight
			FROM line order by turn;

	OPEN line;

	loop_line: LOOP

		FETCH line INTO nome, peso;
		SET soma = soma + peso;
        
        IF (soma > 1000) THEN
			LEAVE loop_line;
		ELSE
			SET nameReturn = nome;
		END IF;

	END LOOP loop_line;
    CLOSE line;

	SELECT nameReturn;

END$$
DELIMITER ;
-- Execucao dos testes
call sp_lastperson_elevator();
-- Alterando a ordem para outro teste
DROP INDEX turn_UNIQUE on line;
update line set turn = 1 where id = 5;
update line set turn = 2 where id = 1;
update line set turn = 3 where id = 6;
update line set turn = 4 where id = 3;
update line set turn = 5 where id = 2;
update line set turn = 6 where id = 4;
ALTER TABLE line ADD CONSTRAINT turn_UNIQUE UNIQUE (turn);

call sp_lastperson_elevator();